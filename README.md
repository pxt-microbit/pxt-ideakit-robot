# micro:bit target for PXT

[![Build Status](https://travis-ci.org/microsoft/pxt-microbit.svg?branch=master)](https://travis-ci.org/microsoft/pxt-microbit) ![pxt-testghpkgs](https://github.com/microsoft/pxt-microbit/workflows/pxt-testghpkgs/badge.svg)

pxt-microbit is a [Microsoft Programming Experience Toolkit (PXT)](https://github.com/Microsoft/pxt) target that allows you to program a [BBC micro:bit](https://microbit.org/). 

* pxt-microbit **beta**, ``v3.0.*`` requires 
  * [pxt-microbit#stable3.0](https://github.com/Microsoft/pxt-microbit/tree/stable3.0)
  * [pxt#stable6.0](https://github.com/Microsoft/pxt/tree/stable6.0).
  * [pxt-common-packages#stable6.0](https://github.com/Microsoft/pxt-common-packages/tree/stable7.0).
* pxt-microbit ``v2.0.*``, branch ``stable2.0``, requires [pxt v5.15.\*](https://github.com/microsoft/pxt/tree/stable5.15). It is the servicing branch for live editor.
* pxt-microbit ``v1.*`` requires pxt v4.4, which is currently in the [stable4.4 branch of pxt](https://github.com/Microsoft/pxt/tree/stable4.4).
* pxt-microbit ``v0.*`` is in the [v0 branch of this repository](https://github.com/microsoft/pxt-microbit/tree/v0)

* [Try it live](https://makecode.microbit.org/)

## Local server setup

The local server lets you to run the editor and serve the documentation from your own computer. It is meant for a single developer used and not designed to serve the editor to a large amount of users.

1. Install [Node.js](https://nodejs.org/) 8.9.4 or higher.
2. Clone this repository.
```
git clone https://gitlab.com/pxt-microbit/pxt-ideakit-robot.git
cd pxt-ideakit-robot
```
3. Install the PXT command line (add `sudo` for Mac/Linux shells).
```
npm install -g pxt
```
4. Install the pxt-microbit dependencies.
```
npm install
```

5. Installs the compiled editor files.
```
pxt target microbit
```

ุ6. Installs package pxt .
```
pxt install
```

Go to the **Running** section.

### Running

Run this command from inside pxt-microbit to open a local web server
```
pxt serve
```
If the local server opens in the wrong browser, make sure to copy the URL containing the local token. 
Otherwise, the editor will not be able to load the projects.

If you need to modify the `.cpp` files (and have installed yotta), enable yotta compilation using the `--localbuild` flag:
```
pxt serve --local
```

If you want to speed up the build, you can use the ``rebundle`` option, which skips building and simply refreshes the target information
```
pxt serve --rebundle
```

### Building local package file
Run this command from inside pxt-microbit to export extension package file
```
pxt build
```
package file inside build

### Cleaning

Sometimes, your built folder might be in a bad state, clean it and try again.
```
pxt clean
```
